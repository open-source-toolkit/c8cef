# Redis 5.0.14 安装包（Linux 和 Windows）

## 简介

本仓库提供 Redis 5.0.14 版本的安装包，适用于 Linux 和 Windows 操作系统。Redis 是一个开源的内存数据结构存储系统，常用作数据库、缓存和消息中间件。

## 资源文件

- **redis-5.0.14-linux.tar.gz**: 适用于 Linux 系统的 Redis 5.0.14 安装包。
- **redis-5.0.14-win.zip**: 适用于 Windows 系统的 Redis 5.0.14 安装包。

## 安装方法

### Linux 系统

1. **下载安装包**:
   ```bash
   wget https://github.com/your-repo/redis-5.0.14-linux.tar.gz
   ```

2. **解压安装包**:
   ```bash
   tar -xzvf redis-5.0.14-linux.tar.gz
   ```

3. **进入解压后的目录**:
   ```bash
   cd redis-5.0.14
   ```

4. **编译并安装**:
   ```bash
   make
   sudo make install
   ```

5. **启动 Redis 服务**:
   ```bash
   redis-server
   ```

### Windows 系统

1. **下载安装包**:
   ```bash
   wget https://github.com/your-repo/redis-5.0.14-win.zip
   ```

2. **解压安装包**:
   ```bash
   unzip redis-5.0.14-win.zip
   ```

3. **进入解压后的目录**:
   ```bash
   cd redis-5.0.14
   ```

4. **启动 Redis 服务**:
   ```bash
   redis-server.exe
   ```

## 注意事项

- 请确保在安装和运行 Redis 之前，系统已经安装了必要的依赖库。
- 在 Windows 系统上，建议使用 PowerShell 或命令提示符来执行上述命令。

## 贡献

欢迎大家提交问题和改进建议。如果你有更好的安装方法或资源文件，也欢迎提交 Pull Request。

## 许可证

本仓库中的资源文件遵循 Redis 的原始许可证。详情请参阅 [LICENSE](LICENSE) 文件。